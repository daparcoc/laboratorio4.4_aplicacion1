package com.daparcoc.laboratorio44_aplicacion1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class OnChargeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("App", "Recibido!");
        Toast.makeText(context, "Ha conectado el cargador.",
                Toast.LENGTH_LONG).show();
    }
}